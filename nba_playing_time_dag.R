library(dagitty)
library(ggdag)

dag <-
  dagitty("dag {X1 -> X2
          X1 -> Y
          X2 -> Y
          X3 -> X2
          X3 -> Y
          X4 -> X2
          X4 -> Y
          X5 -> X2
          X5 -> Y
          X6 -> X2
          X6 -> Y
          }")

coordinates( dag ) <-  list(
  x=c(X1=2, X2=3, X3=1, X4=4, X5=5, X6=5, Y=3),
  y=c(X1=1, X2=3, X3=3, X4=4, X5=3, X6=1, Y=1) )

outcomes(dag) <- c("Y")
exposures(dag) <- c("X2")

tidy_dag <- tidy_dagitty(dag) %>%
  dag_label(labels = c("Y" = "Career Success",
                       "X2" = "NBA Y1 MP",
                       "X1" = "RSCI Ranked",
                       "X3" = "Time in NCAA",
                       "X4" = "NCAA Performance",
                       "X5" = "Draft Features",
                       "X6" = "Physical Features"))
ggdag(tidy_dag, use_labels = "label", text = FALSE) +
  theme_dag() -> p1

# ggdag_adjustment_set(tidy_dag, node_size = 14, text_col = "black",
#                      use_labels = "label", text = FALSE) + 
#   theme_dag() +
#   theme(legend.position = "bottom")
