---
title: "NBA Playing Time Effects"
author: "Sean Fischer"
date: "7/31/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Player development is a crucial aspect of achieving and maintaining success in all sporting domains. In American professional basketball, this process is made challenging by relatively limited roster sizes and a small developmental league. Coupled with regulations limiting contract lengths to no more than five years, these limitations increase the risk associated with personnel decisions, especially those associated with the selection of new players through the NBA draft. As such, player evaluation has received substantial attention within NBA teams and among the sport analytic community. However, little analytic attention has been paid to evaluating factors associated with player development after they are drafted.

In this study I answer whether the amount of playing time an NBA player receives early in their career has a causal effect on the quality of their career. 

## Data
This study was restricted to the analysis of players drafted between 2000 and 2013 who entered the NBA out of college. For each of these players, I collected their per-game and advanced stats from their last year of college play from Sports Reference. These data included their strength of schedule, minutes played, true-shooting percentage, effective-field-goal percentage, and win-shares-per-40. I also collected data on how many minutes these players played in their first and second seasons in the NBA, the length of their NBA careers, and their career win shares and career win-shares-per-48 using the nbastatR package in R [@]. nbastatR collects data from Basketball Reference and the NBA's official stats portal. All continuous measures were standardized in all analyses. Data collection was capped up through 2013 because players drafted after this year had not yet had the opportunity to  begin playing through their rookie extensions.

## Method
The best way to assess the efficacy of any strategic decision is a randomized control trial. However, such a study is not plausible in this case, given that NBA teams are unwilling to randomly assign playing time during their seasons. In its place, I conduct an observational analysis using regression techniques. 

To assess the relationship between early-career playing time and overall career performance, I utilized a regression-based framework for causal inference. I assume that  

```{r dag, echo = FALSE, fig.align='center', dev='svg', warning=FALSE, message=FALSE, fig.cap="Directed acyclic graph (DAG) visualizing the hypothesized relationship between first year playing time, confounders, and career quality."}
source("nba_playing_time_dag.R")
p1
```

## Results
## First Year Minutes Played

### Propensity Score Weighting

### Model Estimates

## Discussion